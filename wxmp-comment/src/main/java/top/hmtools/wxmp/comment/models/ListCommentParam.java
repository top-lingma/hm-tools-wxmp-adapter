package top.hmtools.wxmp.comment.models;

public class ListCommentParam {

	/**
	 * 群发返回的msg_data_id
	 */
	private Long msg_data_id;
	
	/**
	 * 多图文时，用来指定第几篇图文，从0开始，不带默认返回该msg_data_id的第一篇图文
	 */
	private Long index;
	
	/**
	 * 起始位置
	 */
	private Long begin;
	
	/**
	 * 获取数目（>=50会被拒绝）
	 */
	private Long count;
	
	/**
	 * type=0 普通评论&精选评论 type=1 普通评论 type=2 精选评论
	 */
	private Long type;

	public Long getMsg_data_id() {
		return msg_data_id;
	}

	public void setMsg_data_id(Long msg_data_id) {
		this.msg_data_id = msg_data_id;
	}

	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public Long getBegin() {
		return begin;
	}

	public void setBegin(Long begin) {
		this.begin = begin;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	public Long getType() {
		return type;
	}

	public void setType(Long type) {
		this.type = type;
	}
	
	
}
