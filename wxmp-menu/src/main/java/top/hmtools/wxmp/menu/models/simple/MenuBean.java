package top.hmtools.wxmp.menu.models.simple;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MenuBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4412389461451717600L;
	
	protected List<Button> button = new ArrayList<>();
	
	/**
	 * 添加一个按钮（一级菜单），最多只能添加3个，多出的会被忽略
	 * @param buttonBeans
	 */
	public void addButton(Button...buttonBeans ){
		for(Button btn:buttonBeans){
			if(this.button.size()<=3){
				this.button.add(btn);
			}
		}
	}

	/**
	 * 获取按钮（一级菜单）
	 * @return
	 */
	public List<Button> getButton() {
		return button;
	}

	/**
	 * 设置按钮（一级菜单）
	 * @param button
	 */
	public void setButton(List<Button> buttons) {
		if(buttons.size()>3){
			this.button.addAll(buttons.subList(0, 3));
		}else{
			this.button=buttons;
		}
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "MenuBean [button=" + button + "]";
	}

}
