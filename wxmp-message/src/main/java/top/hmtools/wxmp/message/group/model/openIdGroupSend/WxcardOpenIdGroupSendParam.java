package top.hmtools.wxmp.message.group.model.openIdGroupSend;

public class WxcardOpenIdGroupSendParam extends BaseOpenIdGroupSendParam {

	private Wxcard wxcard;

	public Wxcard getWxcard() {
		return wxcard;
	}

	public void setWxcard(Wxcard wxcard) {
		this.wxcard = wxcard;
	}

	@Override
	public String toString() {
		return "WxcardOpenIdGroupSendParam [wxcard=" + wxcard + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
}
