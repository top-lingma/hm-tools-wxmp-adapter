package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * Auto-generated: 2019-08-25 18:7:42
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class ItemList {

	private String content;
	private String id;

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

}