package top.hmtools.wxmp.webpage.authorize;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.AppId;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.webpage.authorize.OAuth2Tools.EScope;

/**
 * 测试网页授权
 * @author HyboWork
 *
 */
public class OAuth2ToolsTest {

	private ObjectMapper objectMapper;
	
	private final Logger logger = LoggerFactory.getLogger(OAuth2ToolsTest.class);






















	/**
	 * 网页授权第一步：获取请求微信网页授权的跳转URL
	 */
	@Test
	public void testGetOAuth2UrlStr() {
		String oAuth2UrlStr = OAuth2Tools.getOAuth2UrlStr(AppId.appid, "http://www.hybomyth.com/aaa/bbb", EScope.SNSAPI_USERINFO, "");
		this.logger.info("请求微信网页授权的URL是：{}",oAuth2UrlStr);
	}

	/**
	 * 网页授权第二步：通过使用微信开发者工具将第一步获取的URL进行跳转授权后，在成功的情况下，可以在地址栏中的重定向URL中获取到“code”参数的值
	 * <br>用第一步获取的code参数值，通过httpclient请求微信服务器，换取accesstoken信息
	 */
	@Test
	public void testGetAccessToken() {
		OAuth2ATBean accessToken = OAuth2Tools.getAccessToken("0012jX5I1qAY7401jS7I164G5I12jX5G", AppId.appid, AppId.appsecret);
		this.printFormatedJson("获取到的accessToken是：", accessToken);
	}

	/**
	 * 网页授权第三步（可选）：对第二步获取的accessToken进行刷新操作，其主要参数“refreshToken”的值来自第二步的返回数据
	 */
	@Test
	public void testRefreshToken() {
		OAuth2ATBean refreshToken = OAuth2Tools.refreshToken(AppId.appid, "33_ZPkrAbcuQjAYzsGjELY272hDJ87t4xlERudRuO7jR8jDzeVPHuCjNkMfihK1eci1KRC6Muwax4z_ZSBOLSUkeg");
		this.printFormatedJson("刷新token", refreshToken);
	}

	/**
	 * 网页授权第四步：通过第二步的操作，使用accesstoken换取用户信息
	 */
	@Test
	public void testGetUserInfo() {
		OAuth2ATBean accessToken = OAuth2Tools.getAccessToken("0012jX5I1qAY7401jS7I164G5I12jX5G", AppId.appid, AppId.appsecret);
		this.printFormatedJson("获取到的accessToken是：", accessToken);
		OAuth2UserBean userInfo = OAuth2Tools.getUserInfo(accessToken);
		this.printFormatedJson("获取到的用户信息是", userInfo);
	}

	/**
	 * 测试 检测accessToken
	 */
	@Test
	public void testCheckAccessToken() {
		ErrcodeBean checkAccessToken = OAuth2Tools.checkAccessToken("33_R2K5NzUbLM2qxOH0cdyCQ4knha49Y1GaOsvpdjhZnCGdLyRShIqEsyqbPSQVTDJVpIdNniuHyEmDZrGNErndwA", "o-OZ0v8HcoPJiNlZLTOZYrkeZUG0");
		this.printFormatedJson("检查accesstoken", checkAccessToken);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			//阿里的fastjson具有很好的兼容性，所以才多次一举
			String jsonString = JSON.toJSONString(obj);
			Object tempObj = JSON.parse(jsonString);
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(tempObj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
}
