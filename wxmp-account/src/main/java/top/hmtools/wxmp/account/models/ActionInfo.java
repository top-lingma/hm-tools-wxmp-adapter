package top.hmtools.wxmp.account.models;

/**
 * 二维码详细信息
 * @author Hybomyth
 *
 */
public class ActionInfo {

	/**
	 * 场景详情
	 */
	private Scene scene;

	public Scene getScene() {
		return scene;
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	@Override
	public String toString() {
		return "ActionInfo [scene=" + scene + "]";
	}
	
	
}
