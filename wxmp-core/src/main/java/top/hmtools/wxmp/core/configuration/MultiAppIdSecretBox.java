package top.hmtools.wxmp.core.configuration;

import java.util.HashMap;
import java.util.Map;

public class MultiAppIdSecretBox implements AppIdSecretBox {
	
	/**
	 * 微信appid与appsecret数据对字典
	 */
	private static HashMap<String,String> appIdSecretPairs;
	
	static{
		//初始化 appid与appsecret数据对字典
		if(appIdSecretPairs == null){
			synchronized (WxmpConfiguration.class) {
				if(appIdSecretPairs == null){
					appIdSecretPairs = new HashMap<>();
				}
			}
		}
	}
	
	/**
	 * 添加微信appid与appsecret数据对到字典
	 * @param pair
	 */
	public void addAppidSecretPair(AppIdSecretPair pair){
		if(pair == null){
			return;
		}
		
		this.addAppidSecretPair(pair.getAppid(), pair.getAppsecret());
	}
	
	/**
	 * 添加微信appid与appsecret数据对到字典
	 * @param appid
	 * @param appsecret
	 */
	public void addAppidSecretPair(String appid,String appsecret){
		if(appid == null || appid.trim().length() < 1){
			return;
		}
		if(appsecret == null || appsecret.trim().length() < 1){
			return;
		}
		
		appIdSecretPairs.put(appid, appsecret);
	}
	
	/**
	 * 添加全部微信appid与appsecret数据对到字典
	 * @param pairs
	 */
	public void addAllAppidSecretPairs(Map<String,String> pairs){
		if(pairs == null){
			return;
		}
		appIdSecretPairs.putAll(pairs);
	}
	
	/**
	 * 移除一条微信appid与appsecret数据对
	 * @param appid
	 */
	public void removeAppid(String appid){
		if(appid == null){
			return;
		}
		appIdSecretPairs.remove(appid);
	}
	
	/**
	 * 清空微信appid与appsecret数据对字典
	 */
	public void removeAllAppid(){
		appIdSecretPairs.clear();
	}
	
	

	@Override
	public AppIdSecretPair getAppIdSecretPair() {
		// TODO Auto-generated method stub
		return null;
	}

}
