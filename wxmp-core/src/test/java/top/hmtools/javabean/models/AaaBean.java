package top.hmtools.javabean.models;

import java.io.File;

public class AaaBean {

	private String aaaName;
	
	private File file;
	
	private BbbBean bbbBean;

	public String getAaaName() {
		return aaaName;
	}

	public void setAaaName(String aaaName) {
		this.aaaName = aaaName;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public BbbBean getBbbBean() {
		return bbbBean;
	}

	public void setBbbBean(BbbBean bbbBean) {
		this.bbbBean = bbbBean;
	}

	@Override
	public String toString() {
		return "AaaBean [aaaName=" + aaaName + ", file=" + file + ", bbbBean=" + bbbBean + "]";
	}
	
	
}
