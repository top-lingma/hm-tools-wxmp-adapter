package top.hmtools.wxmp;

import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import top.hmtools.wxmp.core.WxmpSession;
import top.hmtools.wxmp.core.WxmpSessionFactory;
import top.hmtools.wxmp.core.WxmpSessionFactoryBuilder;
import top.hmtools.wxmp.core.access_handle.DefaultAccessTokenHandle;
import top.hmtools.wxmp.core.configuration.AppIdSecretBox;
import top.hmtools.wxmp.core.configuration.AppIdSecretPair;
import top.hmtools.wxmp.core.configuration.WxmpConfiguration;

public class BaseTest {
	
	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	
	protected WxmpSession wxmpSession;
	
	protected WxmpSessionFactory factory;
	
	private  ObjectMapper objectMapper;

	@Before
	public void init(){
		//TODO 必要时，可以对 httpclient进行设置
//		HmHttpClientFactoryHandle.setConnectionKeepAliveStrategy(connectionKeepAliveStrategy);
		//…………
		
		//初始化全局配置对象实例
		WxmpConfiguration configuration = new WxmpConfiguration();
		
		//设置 存储appid，appsecret 数据对 的盒子
		AppIdSecretBox appIdSecretBox = new AppIdSecretBox() {
			
			@Override
			public AppIdSecretPair getAppIdSecretPair() {
				AppIdSecretPair appIdSecretPair = new AppIdSecretPair();
				appIdSecretPair.setAppid(AppId.appid);
				appIdSecretPair.setAppsecret(AppId.appsecret);
				return appIdSecretPair;
			}
		};
		
		//设置 获取access token 的中间件
		DefaultAccessTokenHandle accessTokenHandle = new DefaultAccessTokenHandle(appIdSecretBox);
		configuration.setAccessTokenHandle(accessTokenHandle);
		
		//获取session工厂对象实例
		this.factory = WxmpSessionFactoryBuilder.build(configuration);
		//获取session对象实例
		this.wxmpSession = this.factory.openSession();
	}
	
	
	
	/**
	 * 格式化打印json字符串到控制台
	 * @param title
	 * @param obj
	 */
	protected synchronized void printFormatedJson(String title,Object obj) {
		if(this.objectMapper == null){
			this.objectMapper = new ObjectMapper();
		}
		try {
			String formatedJsonStr = this.objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			this.logger.info("\n{}：\n{}",title,formatedJsonStr);
		} catch (JsonProcessingException e) {
			this.logger.error("格式化打印json异常：",e);
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
