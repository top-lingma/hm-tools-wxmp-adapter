package top.hmtools.wxmp.server.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletInputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class MessageReceiveController extends BaseController{

	/**
	 * 接收微信公众号推送消息
	 * @return
	 * @throws IOException
	 */
	@RequestMapping("/wxmp/hello")
	@ResponseBody
	public String sayHelloToWxmp() throws IOException{
		//提取微信公众号服务器发送过来的hello参数
		Map<String, String[]> parameterMap = this.getRequest().getParameterMap();
		this.printFormatedJson("微信say hello 的参数", parameterMap);
		
		//向微信公众号say hello
		String echostr = this.getRequest().getParameter("echostr");
		if(StringUtils.isNotEmpty(echostr)){
			return echostr;
		}
		
		//提取事件消息
		ServletInputStream inputStream = this.getRequest().getInputStream();
		String eventMsgString = IOUtils.toString(inputStream,"utf-8");
		this.logger.info("事件消息：{}",eventMsgString);
		return "";
	}
}
